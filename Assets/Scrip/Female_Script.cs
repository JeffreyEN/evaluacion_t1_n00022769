using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Female_Script : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    public Pj_Scrip Pj_Scrip;

    public int VelocidadDeMovimiento;
    public float upSpeed = 60;
    public float RunSpeed=10;
    private bool contador = true;
    private bool EstaTocandoElSuelo = false;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);

        if (Pj_Scrip.muerto == false)
        {
            if (contador)
            {
                sr.flipX = false;
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
                if(EstaTocandoElSuelo)
                    rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }
            else
            {
                sr.flipX = true;
                rb2d.velocity = new Vector2(-RunSpeed, rb2d.velocity.y);
                if (EstaTocandoElSuelo)
                    rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }
        }
        else
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;

        if (contador)
            contador = false;
        else
            contador = true;

    }
}
