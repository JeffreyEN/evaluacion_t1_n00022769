using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie_Male_Scrip : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    public Pj_Scrip Pj_Scrip;

    
    public float upSpeed = 60;

    private bool EstaTocandoElSuelo = false;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Pj_Scrip.muerto == false && EstaTocandoElSuelo)
        {
            rb2d.velocity = Vector2.up * upSpeed;
            EstaTocandoElSuelo = false;
        }
        else
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;
    }
}

