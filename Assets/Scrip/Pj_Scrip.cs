using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pj_Scrip : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;

    public float upSpeed = 60f;
    public float RunSpeed = 10f;
    private int Points = 0;

    public bool muerto = false;
    private bool EstaTocandoElSuelo = false;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Debug.Log("Zombies: "+Points);
        if (muerto == false)
        {

            if (Input.GetKey(KeyCode.Space) && EstaTocandoElSuelo)
            {
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }

            else
            {
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
                setRunAnimation();
            }

        }
        else
        {
            setJDeadAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;

        if (other.gameObject.layer == 3)
        {
            muerto = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 6)
        {
            Destroy(other.gameObject);
            Points += 1;
        }
    }

    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setJDeadAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }

}
